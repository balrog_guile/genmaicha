/* =============================================================================
 * お客様情報エリア
 * ========================================================================== */
$(document).ready(function(){
    
    
    // ----------------------------------------------------
    /**
     * タブ切り替え
     */
    (function(){
        var i = 0;
        while( i < $('.nav-tabs li[role=presentation]').length )
        {
            $('.nav-tabs li[role=presentation]').eq(i).find('a').on('click',function(){
                var name = $(this).attr('href').replace(/^\#/,'');
                $('.nav-tabs li[role=presentation]').removeClass('active');
                $(this).parent().addClass('active');
                $('.tab-panels').hide();
                $('.tab-panels#'+name).show();
                return false;
            });
            i ++ ;
        }
        var defulatActive = $('.nav-tabs li[role=presentation].active a').attr('href').replace(/^\#/,'');
        $('.tab-panels').hide();
        $('.tab-panels#'+defulatActive).show();
    })();
    // ----------------------------------------------------
    
});