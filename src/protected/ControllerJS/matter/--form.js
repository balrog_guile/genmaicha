/* =============================================================================
 * 案件フォーム
 * ========================================================================== */
$(document).ready(function(){
    
    // =========================================================================
    /**
     * 基本のバインドのモデル
     */
    var viewModel = function(){
        
        var self = this;
        
        /* データバインド */
        this.taxDetail = [];
        this.matter = function(){};
        
        // ----------------------------------------------------
        /**
         * 課税明細行の書き換えキック
         */
        this.kickstart = ko.observable( false );
        // ----------------------------------------------------
        /**
         * 金額
         */
        
        // ----------------------------------------------------
        
    };
    
    // ----------------------------------------------------
    
    var bind = new viewModel();
    
    
    // =========================================================================
    /**
     * 課税項目のモデル
     */
    
    var taxDetailSouceModel = function(){
        
        var that = this;
        
        // ----------------------------------------------------
        /**
         * コンストラクタ
         */
        (function(){
            var url = koUrl + 'getColumns';
            var params = { 'table': 'tax_detail' };
            $.ajax({
                'url': url,
                'type': 'get',
                'dataType': 'json',
                'data': params,
                'success': function(data){
                    var set = new taxDetailSouceModel();
                    for( var i in data )
                    {
                        if( data[i] == 'matter_id' ){
                            set[ data[i] ] = ko.observable( matterId );
                        }
                        else{
                            set[ data[i] ] = ko.observable('');
                        }
                    }
                    taxDetailSouce.push( set );
                }
            })
        })();
        
        // ----------------------------------------------------
        
        /**
         * 金額変更
         */
        this.kickstart = ko.computed(function(){
            
            return true;
        });
        
        // ----------------------------------------------------
        
        /**
         * 除外リスト
         */
        this.outArea = function(name){
            if(
               ( name == 'setDelete')||
               ( name == 'setRowDetailTax')
            ){
                return true;
            }
            return false;
        };
        
        // ----------------------------------------------------
        /**
         * 削除
         */
        this.setDelete = function(){
            
            var url = koUrl + 'delete';
            var params = {};
            params.model = 'TaxDetailModel';
            params.attribtes = {};
            params.id_columns = 'id';
            for( var name in this )
            {
                //メソッドは読み飛ばし
                if( that.outArea(name) ){continue;}
                params.attribtes[name] = this[name];
            }
            
            //登録されていなければ削除
            if( that.id() == 0 )
            {
                var index = bind.taxDetail.indexOf(that);
                bind.taxDetail.splice(index,1);
                return;
            }
            
            //ajax
            $.ajax({
                'url': url,
                'type': 'post',
                'dataType': 'json',
                'data': params,
                'success': function(data){
                    //登録失敗時
                    if(data.result == false)
                    {
                        alert('削除に失敗しました');
                    }
                    //削除成功
                    else{
                        var index = bind.taxDetail.indexOf(that);
                        bind.taxDetail.splice(index,1);
                    }
                },
                'error': function(e){
                    alert('エラー');
                }
            });
            
        };
            
        // ----------------------------------------------------
        /**
         * アップデート
         */
        this.setRowDetailTax = function(){
            
            //アップデート
            if( this.id() > 0 ){
                var url = koUrl + 'update';
            }
            
            //作成
            else{
                 var url = koUrl + 'create';
            }
            
            
            //パラメータ
            var params = {};
            params.model = 'TaxDetailModel';
            params.attribtes = {};
            params.id_columns = 'id';
            for( var name in this )
            {
                //メソッドは読み飛ばし
                if( that.outArea(name) ){continue;}
                params.attribtes[name] = this[name];
            }
            
            //ajax
            $.ajax({
                'url': url,
                'type': 'post',
                'dataType': 'json',
                'data': params,
                'success': function(data){
                    //登録失敗時
                    if(data.result == false)
                    {
                        var errorMessage = [];
                        for( var name in data.errorMessage )
                        {
                            for( var i in data.errorMessage[name] )
                            {
                                errorMessage.push( data.errorMessage[name][i] );
                            }
                        }
                        alert( 'エラー発生！登録されていませんので注意して下さい' + "\n" + errorMessage.join("\n") );
                    }
                    //登録成功
                    else{
                        for( var name in data.data )
                        {
                            //if( name == 'sub_total'){ continue; }
                            that[name]( data.data[name] );
                        }
                        bind.matter.update();
                    }
                }
            });
        };
        
        // ----------------------------------------------------
    };
    
    
    // =========================================================================
    /**
     * 案件モデル
     */
    var MatterModelSource = function(){
        
        var that = this;
        
        //最初の計算処理チェック
        this.first_calc = false;
        
        // ----------------------------------------------------
       
        /**
         * 除外リスト
         */
        this.outArea = function(name){
            if(
               ( name == 'update')||
               ( name == 'setRowDetailTax')
            ){
                return true;
            }
            return false;
        };
        
        // ----------------------------------------------------
        /**
         * アップデート
         */
        this.update = function(){
            var url = koUrl + 'update';
            
            //パラメータ
            var params = {};
            params.model = 'MatterModel';
            params.attribtes = {};
            params.id_columns = 'id';
            for( var name in this )
            {
                //メソッドは読み飛ばし
                if( that.outArea(name) ){continue;}
                params.attribtes[name] = this[name];
            }
            
            //ajax
            $.ajax({
                'url': url,
                'type': 'post',
                'dataType': 'json',
                'data': params,
                'success': function(data){
                    //登録失敗時
                    if(data.result == false)
                    {
                        var errorMessage = [];
                        for( var name in data.errorMessage )
                        {
                            for( var i in data.errorMessage[name] )
                            {
                                errorMessage.push( data.errorMessage[name][i] );
                            }
                        }
                        alert( 'エラー発生！案件登録失敗' + "\n" + errorMessage.join("\n") );
                    }
                }
            });
        };
        // ----------------------------------------------------
        
    };
    
    // =========================================================================
    
    
    // ----------------------------------------------------
    
    /**
     * 課税明細行の準備
     */
    var taxDetail = [];
    for( var i in taxDetailSouce )
    {
        (function(){
            var set = new taxDetailSouceModel();
            var row = taxDetailSouce[i];
            for( var column in row ){
                //if( column == 'sub_total'){ continue; }
                set[column] = ko.observable( row[column] );
            }
            taxDetail.push( set );
        })();
    }
    
    // ----------------------------------------------------
    
    /**
     * 案件モデルの準備
     */
    var matterSet = new MatterModelSource();
    for( var name in matterSource )
    {
        //if( name != 'tax_subtotal' )
        //{
            matterSet[name] = ko.observable( matterSource[name] );
        //}
    }
    //合計
    /*
    matterSet.tax_subtotal = ko.computed(function(){
        var i = 0;
        var ret = 0;
        while( i <  bind.taxDetail.length )
        {
            var row = bind.taxDetail[i];
            ret += row.sub_total();
            i ++ ;
        }
        ret -= matterSet.tax_discount();
        
        //2回目以降の計算でDB更新
        if( matterSet.first_calc == false )
        {
            matterSet.first_calc = true;
        }
        else{
            matterSet.update();
        }
        
        return ret;
    },matterSet);
    */
    
    // ----------------------------------------------------
    /**
     * KO用へアペンド
     */
    taxDetailSouce = ko.observableArray(taxDetail);
    bind.taxDetail = taxDetailSouce;
    bind.matter = matterSet;
    
    // ----------------------------------------------------
    
    /**
     * 課税明細行を追加
     */
    viewModel.prototype.addDetailTax = function(){
        var url = koUrl + 'getColumns';
        var params = { 'table': 'tax_detail' };
        $.ajax({
            'url': url,
            'type': 'get',
            'dataType': 'json',
            'data': params,
            'success': function(data){
                var set = new taxDetailSouceModel();
                for( var i in data )
                {
                    /*
                    if( data[i] == 'sub_total' ){
                        continue;
                    }
                    */
                    //else if( data[i] == 'matter_id' ){
                    if( data[i] == 'matter_id' ){
                        set[ data[i] ] = ko.observable( matterId );
                    }
                    else{
                        set[ data[i] ] = ko.observable('');
                    }
                }
                /*
                set.sub_total_kick = ko.computed(function(){
                    var ret = set.qty() * set.price();
                    set.sub_total(ret);
                    //bind.calc();
                    return bind.taxDetail;
                },set);
                */
                taxDetailSouce.push( set );
            }
        })
    };
    
    // ----------------------------------------------------
    /**
     * バインド実行
     */
    ko.applyBindings( bind );
    // ----------------------------------------------------
});