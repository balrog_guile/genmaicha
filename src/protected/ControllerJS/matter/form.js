/* =============================================================================
 * 案件フォーム
 * ========================================================================== */
$(document).ready(function(){
    
    // ----------------------------------------------------
    /**
     * ViewModelの準備
     */
    var ViewModel = function(){
        
        var self = this;
        this.nextTime = false;
        this.taxDetailRows = ko.observableArray([]);
        this.noTaxDetailRows = ko.observableArray([]);
        this.matter = {};
        
        // ----------------------------------------------------
        
        /**
         * 合計計算
         */
        this.kickstartFlag = ko.observable(false);
        this.selfchecker = false;
        this.kickstart = ko.computed(function(){
            
            //初回は蹴る
            var check = self.kickstartFlag();
            if( self.nextTime == false ){ return true; }
            if( this.selfchecker == check ){ return true; }
            this.selfchecker = check;
            console.log(check);
            
            //合計額計算
            var grandTotal = 0;
            
            //課税項目
            var taxSuntotal = 0;
            var i = 0;
            while( i < self.taxDetailRows().length )
            {
                var row = self.taxDetailRows()[i];
                if( row.targeting() == 1 ){
                    taxSuntotal += parseFloat(row.sub_total());
                }
                i ++ ;
            }
            taxSuntotal -= parseFloat( self.matter.tax_discount() );
            self.matter.tax_subtotal( taxSuntotal );
            grandTotal += taxSuntotal;
            
            
            //非課税項目
            var noTaxSubtotal = 0;
            var i = 0;
            while( i < self.noTaxDetailRows().length )
            {
                var row = self.noTaxDetailRows()[i];
                if( row.targeting() == 1 ){
                    noTaxSubtotal += parseFloat(row.sub_total());
                }
                i ++ ;
            }
            noTaxSubtotal -= parseFloat( self.matter.notax_discount() );
            self.matter.notax_subtotal( noTaxSubtotal );
            grandTotal += noTaxSubtotal;
            
            
            /////その他合計項目
            
            //消費税
            if( self.matter.is_tax_calc() == 1 )
            {
                var tax = taxSuntotal * ( parseFloat( self.matter.tax_rate() ) / 100 );
                switch( self.matter.tax_mode() )
                {
                    case 1:
                        tax = Math.floor(tax);
                        break;
                    
                    case 2:
                        tax = Math.ceil(tax);
                        break;
                    
                    case 3:
                        tax = Math.round(tax);
                        break;
                }
                self.matter.tax( tax );
            }
            else{
                self.matter.tax(0);
            }
            
            
            //総合計
            grandTotal += tax;
            if( self.matter.is_total_calc() == 1 ){
                self.matter.grand_total( grandTotal );
            }
            else{
                self.matter.grand_total( 0 );
            }
            
            
            //支払い総合計
            if( self.matter.is_total_calc() == 1 ){
                self.matter.payment_total( grandTotal - self.matter.advanced_pay() );
            }
            else{
                self.matter.payment_total( 0 );
            }
            
            
            //////データベースを書き換えに行く
            var dataParmas = {};
            
            //基礎データ
            dataParmas.matter = {};
            var i = 0;
            while( i < self.matter.attributeTarget.length)
            {
                dataParmas.matter[ self.matter.attributeTarget[i] ]
                    = self.matter[ self.matter.attributeTarget[i] ]();
                
                if( Number.isNaN( dataParmas.matter[ self.matter.attributeTarget[i] ] ) )
                {
                    dataParmas.matter[ self.matter.attributeTarget[i] ] = '数値でない';
                }
                i ++ ;
            }
            
            //課税明細行
            dataParmas.taxDetail = [];
            var i = 0;
            while( i < self.taxDetailRows().length )
            {
                var set = {};
                var row = self.taxDetailRows()[i];
                var x = 0;
                while( x < row.attributeTarget.length )
                {
                    var name = row.attributeTarget[x];
                    set[ name ] = row[name]();
                    if( Number.isNaN(set[ name ]) )
                    {
                        set[ name ] = '数値でない';
                    }
                    x ++ ;
                }
                dataParmas.taxDetail.push( set );
                i ++ ;
            }
            
            
            
            //非課税明細行
            dataParmas.noTaxDetail = [];
            var i = 0;
            while( i < self.noTaxDetailRows().length )
            {
                var set = {};
                var row = self.noTaxDetailRows()[i];
                var x = 0;
                while( x < row.attributeTarget.length )
                {
                    var name = row.attributeTarget[x];
                    set[ name ] = row[name]();
                    if( Number.isNaN(set[ name ]) )
                    {
                        set[ name ] = '数値でない';
                    }
                    x ++ ;
                }
                dataParmas.noTaxDetail.push( set );
                i ++ ;
            }
            
            //console.log( $.stringify( dataParmas ) );
            
            //保存しに行く
            var params = {
                'json': $.stringify( dataParmas )
            };
            $.ajax({
                'url': saveUrl,
                'type': 'post',
                'dataType': 'json',
                'data': params,
                'success': function(data){
                    
                    //基本情報分
                    if( data.matter.result == true )
                    {
                        for( var name in data.matter.data )
                        {
                            self.matter[name]( data.matter.data[name] );
                        }
                    }
                    else
                    {
                        
                    }
                    
                    
                    //課税明細
                    var i = 0;
                    var len = data.taxDetail.length;
                    while( i < len )
                    {
                        var ret = data.taxDetail[i];
                        var row = self.taxDetailRows()[i];
                        
                        //現在のエラー削除
                        row.errors.removeAll();
                        
                        //エラーフラグの処理
                        if( ret['result'] == true )
                        {
                            row.isError( false );
                            for( var name in ret['data'] )
                            {
                                if( ret['data'][name] == 0 ){
                                    ret['data'][name] = ~~ ret['data'][name];
                                }
                                row[name]( ret['data'][name] );
                            }
                            
                        }
                        else{
                            row.isError( true );
                            for( var name in ret['errors'] )
                            {
                                var y = 0;
                                while( y < ret['errors'][name].length )
                                {
                                    row.errors.push( ret['errors'][name][y] );
                                    y ++ ;
                                }
                            }
                        }
                        
                        i ++ ;
                    }
                    
                    
                    
                    //非課税明細
                    var i = 0;
                    var len = data.noTaxDetail.length;
                    while( i < len )
                    {
                        var ret = data.noTaxDetail[i];
                        var row = self.noTaxDetailRows()[i];
                        
                        //現在のエラー削除
                        row.errors.removeAll();
                        
                        //エラーフラグの処理
                        if( ret['result'] == true )
                        {
                            row.isError( false );
                            for( var name in ret['data'] )
                            {
                                if( name == 'targeting' ){
                                    ret['data'][name] = ~~ ret['data'][name];
                                }
                                row[name]( ret['data'][name] );
                            }
                            
                        }
                        else{
                            row.isError( true );
                            for( var name in ret['errors'] )
                            {
                                var y = 0;
                                while( y < ret['errors'][name].length )
                                {
                                    row.errors.push( ret['errors'][name][y] );
                                    y ++ ;
                                }
                            }
                        }
                        i ++ ;
                    }
                    
                    
                    
                },
                'error': function(data){
                    alert('データ通信エラー');
                }
            });
            
            
            return true;
        });
        // ----------------------------------------------------
        
        /**
         * 課税明細行を追加
         */
        this.taxRowAdd = function(){
            var set = new self.taxDeatilSource();
            set.rank(self.taxDetailRows().length+1);
            self.taxDetailRows.push( set );
        }
        // ----------------------------------------------------
        /**
         * 課税明細行を追加
         */
        this.noTaxRowAdd = function(){
            var set = new self.taxDeatilSource();
            set.rank(self.noTaxDetailRows().length+1);
            set.noTaxMode = true;
            self.noTaxDetailRows.push( set );
        }
        // ----------------------------------------------------
        
        
        
        /* ==============================================*/
        /* ==============================================*/
        /**
         * 課税明細行のモデル
         */
        this.taxDeatilSource = function(){
            
            var that = this;
            this.attributeTarget = [];
            this.errors = ko.observableArray([]);
            this.isError = ko.observable(false);
            this.noTaxMode = false;
            
            // ----------------------------------------------------
            /**
             * 以下コンストラクタ内容
             */
            var url = koUrl + 'getColumns';
            var params = { 'table': 'tax_detail' };
            $.ajax({
                'url': url,
                'type': 'get',
                'dataType': 'json',
                'data': params,
                'async': false,
                'success': function(data){
                    for( var i in data )
                    {
                        that.attributeTarget.push( data[i] );
                        if( data[i] == 'matter_id' ){
                            that[ data[i] ] = ko.observable( matterId );
                        }
                        else{
                            that[ data[i] ] = ko.observable(null);
                        }
                    }
                }
            });
            
            // ----------------------------------------------------
            
            /**
             * 順番変更
             */
            this.rankChange = function(data, event){
                
                var mode = $(event.currentTarget).attr('value');
                if( that.noTaxMode == true ){
                    var target = self.noTaxDetailRows;
                }
                else{
                    var target = self.taxDetailRows;
                }
                var index = target.indexOf( that );
                
                if( ( mode == '▲' ) && ( index == 0 ) )
                {
                    return;
                }
                if( ( mode == '▼' ) && ( index == (target().length-1) ) )
                {
                    return;
                }
                
                if( mode == '▲' ){
                    target()[index-1].rank(index+1);
                    target()[index].rank(index);
                }
                if( mode == '▼' ){
                    target()[index+1].rank(index+1);
                    target()[index].rank(index+2);
                }
                
                
                target.sort(function( left, right ){
                    if( left.rank() == right.rank() )
                    {
                        return 0;
                    }
                    if( left.rank() < right.rank() )
                    {
                        return -1;
                    }
                    if( left.rank() > right.rank() )
                    {
                        return 1;
                    }
                });
                
                //親を含めて起動
                var ret = ~~( new Date().getTime() / 1000 );
                if( self.nextTime == true )
                {
                    self.kickstartFlag( ret );
                }
                
            };
            // ----------------------------------------------------
            
            /**
             * 行削除
             */
            this.removeThis = function(){
                var changeTarget;
                if( that.noTaxMode == true )
                {
                    changeTarget = self.noTaxDetailRows;
                    var index = self.noTaxDetailRows.indexOf(that);
                    var row = self.noTaxDetailRows.splice( index, 1 );
                    var params = {
                        'target': 'NoTaxDetailModel',
                        'id': that.id()
                    };
                }
                else{
                    changeTarget = self.taxDetailRows;
                    var index = self.taxDetailRows.indexOf(that);
                    var row = self.taxDetailRows.splice( index, 1 );
                    var params = {
                        'target': 'TaxDetailModel',
                        'id': that.id()
                    };
                }
                var res = '';
                $.ajax({
                    'async': false,
                    'url': rowRemoveUrl,
                    'data': params,
                    'dataType': 'json',
                    'type': 'post',
                    'success': function(data){
                        if( data == true )
                        {
                            var i = 0;
                            while( i < changeTarget().length )
                            {
                                var row = changeTarget()[i];
                                row.rank( i + 1 );
                                i ++ ;
                            }
                            
                            //親を含めて起動
                            var ret = ~~( new Date().getTime() / 1000 );
                            if( self.nextTime == true )
                            {
                                self.kickstartFlag( ret );
                            }
                        }
                        else{
                            alert('削除失敗');
                        }
                        res = true;
                    }
                });
                
                return res;
            };
            
            // ----------------------------------------------------
            
            /**
             * 合計計算用
             */
            this.kickstart = ko.computed(function(){
                var set = that.name();
                var set2 = that.remark();
                var targeting = that.targeting();
                that.sub_total( that.price() * that.qty() );
                //親を含めて起動
                var ret = ~~( new Date().getTime() / 1000 );
                if( self.nextTime == true )
                {
                    self.kickstartFlag( ret );
                }
                return ret;
            });
            
            // ----------------------------------------------------
            
        };
        /* ==============================================*/
        /* ==============================================*/
        
        
        
        
        /* ==============================================*/
        /* ==============================================*/
        
        /**
         * 基本データモデル
         */
        self.matterSource = function(){
            
            var that = this;
            that.attributeTarget = [];
            
            //基本データ
            for( var name in matterSource )
            {
                that[name] = ko.observable( matterSource[name] );
                that.attributeTarget.push( name );
            }
            
            //変更キックスタート
            this.kickstart = ko.computed(function(){
                var ret = new Date().getTime();
                var test = that.tax_discount();
                var test = that.notax_discount();
                var test = that.advanced_pay();
                if( self.nextTime == true )
                {
                    self.kickstartFlag( ret );
                }
                return ret;
            });
        };
        
        /* ==============================================*/
        /* ==============================================*/
        
        
        //初期状態
        (function(){
            
            //課税明細行
            var i = 0;
            while( i < taxDetailSouce.length )
            {
                var row = taxDetailSouce[i];
                var set = new self.taxDeatilSource();
                for( var name in row )
                {
                    if( row[name] == 0 ){
                        row[name] = ~~row[name];
                    }
                    set[name]( row[name] );
                }
                self.taxDetailRows.push( set );
                i ++ ;
            }
            
            
            //非課税明細行
            var i = 0;
            while( i < noTaxDetailSouce.length )
            {
                var row = noTaxDetailSouce[i];
                var set = new self.taxDeatilSource();
                for( var name in row )
                {
                    if( row[name] == 0 ){
                        row[name] = ~~row[name];
                    }
                    set[name]( row[name] );
                }
                set.noTaxMode = true;
                self.noTaxDetailRows.push( set );
                i ++ ;
            }
            
            //基本データ
            self.matter = new self.matterSource();
            
            
            //初回計算を終了
            self.nextTime = true;
        })();
        /* ==============================================*/
        
        
        
        
    }
    /* ==============================================*/
    
    
    // ----------------------------------------------------
    /**
     * KOにバインド
     */
    var bind = new ViewModel();
    ko.applyBindings( bind );
    // ----------------------------------------------------
    
});