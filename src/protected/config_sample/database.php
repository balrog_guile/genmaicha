<?php
/* =============================================================================
 * データベース設定
 * ========================================================================== */
return array(
	'connectionString' => 'mysql:host=localhost;dbname=testdrive',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
);