<?php
ini_set( 'display_errors', 1 );
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'玄米茶',
	'theme' => 'bootstrap',
	'language' => 'ja',
	
	// preloading 'log' component
	'preload'=>array('log'),
	
	'aliases' => array(
		'bootstrap' => 'application.modules.bootstrap',
		'chartjs'   => 'application.modules.bootstrap.extensioins.yii-chartjs-master'
	),
	
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.vendor.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'bootstrap.*',
		'bootstrap.behaviors.*',
		'bootstrap.gii.*',
		'bootstrap.components.*',
		'bootstrap.widgets.*',
		'bootstrap.helpers.*',
		'chartjs.*',
		'chartjs.widgets.*',
		'chartjs.components.*',
	),
	
	'modules'=>array(
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'7410asdf',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
		'user'=>array(
			# encrypting method (php hash function)
			'hash' => 'md5',
			# send activation email
			'sendActivationMail' => true,
			# allow access for non-activated users
			'loginNotActiv' => false,
			# activate user on registration (only sendActivationMail = false)
			'activeAfterRegister' => false,
			# automatically login from registration
			'autoLogin' => true,
			# registration path
			'registrationUrl' => array('/user/registration'),
			# recovery password path
			'recoveryUrl' => array('/user/recovery'),
			# login form path
			'loginUrl' => array('/user/login'),
			# page after login
			'returnUrl' => array('/user/profile'),
			# page after logout
			'returnLogoutUrl' => array('/user/login'),
		),
		'bootstrap' => array(
			'class' => 'bootstrap.BootStrapModule',
		),
	),
	
	// application components
	'components'=>array(
		
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'WebUser',
		),
		
		'session'=>array(
			'class'=>'CDbHttpSession',
			'sessionTableName'=>'sessions',
			'connectionID'=>'db',
		),
		
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'bsHtml' => array('class' => 'bootstrap.components.BSHtml'),
		'chartjs' => array('class' => 'chartjs.components.ChartJs'),
		
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
