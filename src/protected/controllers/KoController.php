<?php
class KoController extends Controller
{
	// ----------------------------------------------------
	
	/**
	 * フィルタリング
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * アクセスコントロール
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('getColumns', 'create','getAll', 'update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * getColumns
	 */
	public function actionGetColumns()
	{
		$connection = Yii::app()->db;//get connection
		$dbSchema = $connection->schema;
		$table = $dbSchema->getTable(Input::Get('table'));
		$column = $table->getColumnNames();
		echo json_encode($column);
	}
	
	// ----------------------------------------------------
	
	/**
	 * create
	 */
	public function actionCreate()
	{
		$modelName = Input::Post('model');
		$attribtes = Input::Post('attribtes');
		$model = new $modelName;
		
		if( isset($attribtes['create_date']) )
		{
			$attribtes['create_date'] = date('Y-m-d H:i:s');
		}
		if( isset($attribtes['update_date']) )
		{
			$attribtes['update_date'] = date('Y-m-d H:i:s');
		}
		$model->attributes = $attribtes;
		
		
		if( $model->save() )
		{
			$get = $modelName::model()->findByPk( $model->id );
			$return = array(
				'result' => true,
				'errorMessage' => array(),
				'data' => $get->attributes
			);
		}
		else
		{
			$return = array(
				'result' => false,
				'errorMessage' => $model->getErrors(),
				'data' => array(),
			);
		}
		echo json_encode($return);
	}
	// ----------------------------------------------------
	/**
	 * create
	 */
	public function actionUpdate()
	{
		$modelName = Input::Post('model');
		$attribtes = Input::Post('attribtes');
		$id_columns = Input::Post('id_columns');
		$model = $modelName::model()->findByPk( $attribtes[$id_columns] );
		unset( $attribtes[$id_columns] );
		
		if( isset($attribtes['update_date']) )
		{
			$attribtes['update_date'] = date('Y-m-d H:i:s');
		}
		$model->attributes = $attribtes;
		
		
		if( $model->save() )
		{
			$return = array(
				'result' => true,
				'errorMessage' => array(),
				'data' => $model->attributes
			);
		}
		else
		{
			$return = array(
				'result' => false,
				'errorMessage' => $model->getErrors(),
				'data' => array(),
			);
		}
		echo json_encode($return);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 削除
	 */
	public function actionDelete()
	{
		$modelName = Input::Post('model');
		$attribtes = Input::Post('attribtes');
		$id_columns = Input::Post('id_columns');
		$model = $modelName::model()->findByPk( $attribtes[$id_columns] );
		unset( $attribtes[$id_columns] );
		if( $model->delete() ){
			$return = array(
				'result' => true
			);
		}
		else{
			$return = array(
				'result' => false
			);
		}
		echo json_encode($return);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 全件取得
	 */
	public function actionGetAll()
	{
		
		
		
		
	}
	
	// ----------------------------------------------------
}