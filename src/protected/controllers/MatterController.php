<?php

class MatterController extends Controller
{
	// ----------------------------------------------------
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	public $assets_helper;


	// ----------------------------------------------------
	
	/**
	 * 初期化
	 */
	public function init() {
		parent::init();
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
	}
	
	// ----------------------------------------------------
	
	/**
	 * フォーム用アセット呼び出し
	 */
	protected function setAssetForm()
	{
		$this->assets_helper->set_js( 'ko.js', 'end' );
		$this->assets_helper->set_js( 'jquery.stringify.js', 'end' );
		$this->assets_helper->set_js( 'matter/form.js', 'end' );
		
	}
	// ----------------------------------------------------
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
					'create','update', 'index','view', 'admin','delete',
					'save', 'DetailRemove', 'document'
				),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Creates a new model.
	 * @param int $cid 顧客ID
	 */
	public function actionCreate( $cid )
	{
		$model=new MatterModel;
		
		/////バリデーション＆SAVE
		if(isset($_POST['MatterModel']))
		{
			
			$_POST['MatterModel']['est_date'] = null;
			$_POST['MatterModel']['est_date'] = null;
			$_POST['MatterModel']['receipt_date'] = null;
			
			
			$model->attributes=$_POST['MatterModel'];
			$model->create_date = date('Y-m-d H:i:s');
			$model->update_date = date('Y-m-d H:i:s');
			
			
			if($model->save())
			{
				$this->redirect(array('update','id'=>$model->id));
			}
			
		}
		//////ページGET
		else
		{
			$model->customer_id = $cid;
		}
		
		
		////顧客
		$customer = CustomerModel::model()->findByPk($cid);
		
		
		
		////関連変数
		$this->assets_helper->add_js(
			'var taxDetail = [];', 
			'var noTaxDetail = [];', 
			'end'
		);
		
		
		////アセット
		$this->setAssetForm();
		
		
		////レンダリング
		$this->render('create',array(
			'model'=>$model,
			'customer' => $customer,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 書類発行
	 * @param string $mode
	 */
	public function actionDocument( $id, $mode = 'bill' )
	{
		$matter = $this->loadModel($id);
		
		Yii::import('application.excel');
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import('application.excel.PHPExcel', true);
		spl_autoload_register(array('YiiBase','autoload')); 
		
		//$reader = PHPExcel_IOFactory::createReader('Excel2007');
		$reader = PHPExcel_IOFactory::createReader('Excel5');
		$excel = $reader->load(
					YiiBase::getPathOfAlias('application.excel_template')
						//. '/bill.xlsx'
						. '/bill.xls'
				);
		
		////親情報を登録
		$excel->setActiveSheetIndex(1);
		$sheet = $excel->getActiveSheet();
		
		//お客様情報
		$sheet->getCell('B1')->setValue( $matter->customer->company_name );
		$sheet->getCell('B2')->setValue( $matter->customer->company_honorific );
		$sheet->getCell('B3')->setValue( $matter->customer->name1 );
		$sheet->getCell('B4')->setValue( $matter->customer->name2 );
		$sheet->getCell('B5')->setValue( $matter->customer->kana1 );
		$sheet->getCell('B6')->setValue( $matter->customer->kana2 );
		$sheet->getCell('B7')->setValue( $matter->customer->honorific );
		$sheet->getCell('B8')->setValue( $matter->customer->pref );
		$sheet->getCell('B9')->setValue( $matter->customer->zip );
		$sheet->getCell('B10')->setValue( $matter->customer->address1 );
		$sheet->getCell('B11')->setValue( $matter->customer->address2 );
		$sheet->getCell('B12')->setValue( $matter->customer->address3 );
		$sheet->getCell('B13')->setValue( $matter->customer->tel1 );
		$sheet->getCell('B14')->setValue( $matter->customer->tel2 );
		$sheet->getCell('B15')->setValue( $matter->customer->fax );
		$sheet->getCell('B16')->setValue( $matter->customer->url );
		$sheet->getCell('B17')->setValue( $matter->customer->email1 );
		$sheet->getCell('B18')->setValue( $matter->customer->email2 );
		
		
		//帳票情報
		$sheet->getCell('B19')->setValue( $matter->est_date );
		$sheet->getCell('B20')->setValue( $matter->bill_date );
		$sheet->getCell('B21')->setValue( $matter->receipt_date );
		$sheet->getCell('B22')->setValue( $matter->delivery_document_date );
		$sheet->getCell('B23')->setValue( $matter->tax_subtotal );
		$sheet->getCell('B24')->setValue( $matter->tax_discount );
		$sheet->getCell('B25')->setValue( $matter->notax_subtotal );
		$sheet->getCell('B26')->setValue( $matter->notax_discount );
		$sheet->getCell('B27')->setValue( $matter->tax );
		$sheet->getCell('B28')->setValue( $matter->advanced_pay );
		$sheet->getCell('B29')->setValue( $matter->grand_total );
		$sheet->getCell('B30')->setValue( $matter->payment_total );
		$sheet->getCell('B31')->setValue( $matter->name );
		$sheet->getCell('B32')->setValue( $matter->est_remark );
		$sheet->getCell('B33')->setValue( $matter->bill_remark );
		$sheet->getCell('B34')->setValue( $matter->receipt_remark );
		$sheet->getCell('B35')->setValue( $matter->delivery_document_remark );
		
		
		
		////明細セル
		$excel->setActiveSheetIndex(2);
		$sheet = $excel->getActiveSheet();
		$taxFirstRow = $sheet->getCell('B1')->getValue();
		$taxCell = $sheet->getCell('B2')->getValue();
		$taxRemarksCell = $sheet->getCell('B3')->getValue();
		$taxPriceCell = $sheet->getCell('B4')->getValue();
		$taxQtyCell = $sheet->getCell('B5')->getValue();
		$taxUnitCell = $sheet->getCell('B6')->getValue();
		$taxSubtotalCell = $sheet->getCell('B7')->getValue();
		$notaxFirstRow = $sheet->getCell('B8')->getValue();
		$notaxCell = $sheet->getCell('B9')->getValue();
		$notaxRemarksCell = $sheet->getCell('B10')->getValue();
		$notaxPriceCell = $sheet->getCell('B11')->getValue();
		$notaxQtyCell = $sheet->getCell('B12')->getValue();
		$notaxUnitCell = $sheet->getCell('B13')->getValue();
		$notaxSubtotalCell = $sheet->getCell('B14')->getValue();
		
		$excel->setActiveSheetIndex(0);
		$sheet = $excel->getActiveSheet();
		
		
		//課税項目明細
		$startCell = $taxFirstRow;
		$add = 0;
		foreach( $matter->taxDetail as $detail )
		{
			$sheet->insertNewRowBefore( $startCell+1, 1 );
			$this->copyRows( $sheet, $taxFirstRow, $startCell, 1, 6 );
			$sheet->getCell($taxCell.$startCell)->setValue( $detail->name );
			$sheet->getCell($taxRemarksCell.$startCell)->setValue( $detail->remark );
			$sheet->getCell($taxPriceCell.$startCell)->setValue( $detail->price );
			$sheet->getCell($taxQtyCell.$startCell)->setValue( $detail->qty );
			$sheet->getCell($taxUnitCell.$startCell)->setValue( $detail->qty_unit );
			$sheet->getCell($taxSubtotalCell.$startCell)->setValue( $detail->sub_total );
			$startCell ++ ;
			$add ++ ;
		}
		$sheet->removeRow($startCell);
		
		//非課税項目明細
		$startCell2 = $notaxFirstRow + ($add-1);
		foreach( $matter->noTaxDetail as $detail )
		{
			$sheet->insertNewRowBefore( $startCell2+1, 1 );
			$this->copyRows( $sheet, ($notaxFirstRow + ($add-1)), $startCell2, 1, 6 );
			$sheet->getCell($notaxCell.$startCell2)->setValue( $detail->name );
			$sheet->getCell($notaxRemarksCell.$startCell2)->setValue( $detail->remark );
			$sheet->getCell($notaxPriceCell.$startCell2)->setValue( $detail->price );
			$sheet->getCell($notaxQtyCell.$startCell2)->setValue( $detail->qty );
			$sheet->getCell($notaxUnitCell.$startCell2)->setValue( $detail->qty_unit );
			$sheet->getCell($notaxSubtotalCell.$startCell2)->setValue( $detail->sub_total );
			$startCell2 ++ ;
		}
		$sheet->removeRow($startCell2);
		
		
		//保存
		$excel->setActiveSheetIndex(0);
		ini_set('memory_limit', '2048M'); set_time_limit('1200');
		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		//$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$path = YiiBase::getPathOfAlias('application.excel_template') . '/test.xlsx';
		//$path = YiiBase::getPathOfAlias('application.excel_template') . '/test.xls';
		$writer->save(
			$path
		);
		
		//ダウンロード
		$fileName = $matter->id . '_' . $mode . '_' .date('Y-m-d_H-i-s'). '.xlsx';
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=". $fileName);
		echo file_get_contents( $path );
		
		unlink($path);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * エクセル行コピー
	 */
	public function copyRows(
		$sheet,//シート情報
		$srcRow,	// 複製元行番号
		$dstRow,	// 複製先行番号
		$height, 	// 複製行数
		$width		// 複製カラム数
	) {
		
		for($row=0; $row<$height; $row++) {
			// セルの書式と値の複製
			for ($col=0; $col<$width; $col++) {
				$cell = $sheet->getCellByColumnAndRow($col, $srcRow+$row);
				$style = $sheet->getStyleByColumnAndRow($col, $srcRow+$row);
				
				$dstCell = PHPExcel_Cell::stringFromColumnIndex($col).(string)($dstRow+$row);
				$sheet->setCellValue($dstCell, $cell->getValue());
				$sheet->duplicateStyle($style, $dstCell);
			}
			
			// 行の高さ複製。
			$h = $sheet->getRowDimension($srcRow+$row)->getRowHeight();
			$sheet->getRowDimension($dstRow+$row)->setRowHeight($h);
		}
		
		// セル結合の複製
		// - $mergeCell="AB12:AC15" 複製範囲の物だけ行を加算して復元。 
		// - $merge="AB16:AC19"
		foreach ($sheet->getMergeCells() as $mergeCell) {
			$mc = explode(":", $mergeCell);
			$col_s = preg_replace("/[0-9]*/" , "",$mc[0]);
			$col_e = preg_replace("/[0-9]*/" , "",$mc[1]);
			$row_s = ((int)preg_replace("/[A-Z]*/" , "",$mc[0])) - $srcRow;
			$row_e = ((int)preg_replace("/[A-Z]*/" , "",$mc[1])) - $srcRow;
			
			// 複製先の行範囲なら。
			if (0 <= $row_s && $row_s < $height) {
				$merge = $col_s.(string)($dstRow+$row_s).":".$col_e.(string)($dstRow+$row_e);
				$sheet->mergeCells($merge);
			}
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$customer = $model->customer;
		
		if(isset($_POST['MatterModel']))
		{
			if( $_POST['MatterModel']['est_date'] == '' ){
				$_POST['MatterModel']['est_date'] = null;
			}
			if( $_POST['MatterModel']['bill_date'] == '' ){
				$_POST['MatterModel']['bill_date'] = null;
			}
			if( $_POST['MatterModel']['receipt_date'] == '' ){
				$_POST['MatterModel']['receipt_date'] = null;
			}
			if( $_POST['MatterModel']['delivery_document_date'] == '' ){
				$_POST['MatterModel']['delivery_document_date'] = null;
			}
			
			$model->attributes=$_POST['MatterModel'];
			$model->update_date = date('Y-m-d H:i:s');
			if($model->save())
			{
				$this->redirect(array('update','id'=>$model->id));
			}
		}
		
		
		////関連変数
		$taxDetail = array();
		foreach( $model->taxDetail as $row )
		{
			$taxDetail[] = $row->attributes;
		}
		$noTaxDetail = array();
		foreach( $model->noTaxDetail as $row )
		{
			$noTaxDetail[] = $row->attributes;
		}
		$this->assets_helper->add_js(
			'
				var taxDetailSouce = ' . json_encode( $taxDetail ) .';
				var noTaxDetailSouce = ' . json_encode( $noTaxDetail ) .';
				var koUrl = "' . Yii::app()->createUrl('ko') .'/";
				var saveUrl = "' . Yii::app()->createUrl('matter/save') .'";
				var matterId = ' . $model->id . ';
				var matterSource = ' . json_encode( $model->attributes ) . ';
				var rowRemoveUrl = "' . Yii::app()->createUrl('matter/DetailRemove') .'";
			', 
			'end'
		);
		
		
		////アセット
		$this->setAssetForm();
		
		
		$this->render('update',array(
			'model'=>$model,
			'customer' => $customer
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MatterModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MatterModel']))
		{
			$model->attributes=$_GET['MatterModel'];
		}
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MatterModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MatterModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * JSONでデータ保存
	 */
	public function actionSave()
	{
		$json = Input::GetPost('json');
		$data = json_decode($json);
		
		$return = array(
			'matter' => array(),
			'taxDetail' => array(),
			'noTaxDetail' => array(),
		);
		
		//基本情報保存
		$matter = $data->matter;
		$matterModel = MatterModel::model()->findByPk($matter->id);
		$matterArray = (array)$matter;
		
		unset( $matterArray['id'] );
		$matterArray['update_date'] = date('Y-m-d H:i:s');
		if( $matterArray['est_date'] == '' || $matterArray['est_date'] == '0000-00-00' )
		{
			$matterArray['est_date'] = null;
		}
		if( $matterArray['bill_date'] == '' || $matterArray['bill_date'] == '0000-00-00' )
		{
			$matterArray['bill_date'] = null;
		}
		if( $matterArray['receipt_date'] == '' || $matterArray['receipt_date'] == '0000-00-00' )
		{
			$matterArray['receipt_date'] = null;
		}
		if( $matterArray['delivery_document_remark'] == '' || $matterArray['delivery_document_remark'] == '0000-00-00' )
		{
			$matterArray['delivery_document_remark'] = null;
		}
		$matterModel->attributes = $matterArray;
		
		$result = $matterModel->save();
		$return['matter']['result'] = $result;
		if( $result === false)
		{
			$return['matter']['errors'] = $matterModel->getErrors();
			$return['matter']['data'] = $matterModel->getAttributes();
		}
		else{
			$return['matter']['errors'] = array();
			$return['matter']['data'] = array();
			foreach( $matterModel->attributeNames() as $name )
			{
				$return['matter']['data'][$name] = $matterModel->{$name};
			}
		}
		
		
		
		
		//行データ
		$taxDetail = $data->taxDetail;
		foreach( $taxDetail as $row )
		{
			if( (int)$row->id > 0 )
			{
				$taxDetailModel = TaxDetailModel::model()->findByPk( $row->id );
				$attr = (array)$row;
				unset( $attr['id'] );
				$attr['update_date'] = date('Y-m-d H:i:s');
			}
			else
			{
				$taxDetailModel = new TaxDetailModel();
				$attr = (array)$row;
				unset( $attr['id'] );
				$attr['create_date'] = date('Y-m-d H:i:s');
				$attr['update_date'] = date('Y-m-d H:i:s');
			}
			
			$taxDetailModel->attributes = $attr;
			
			$set = array();
			$set['result'] = $taxDetailModel->save();
			if( $set['result'] === true )
			{
				$set['errors'] = array();
				$getModel = TaxDetailModel::model()->findByPk($taxDetailModel->id);
				$set['data'] = $getModel->getAttributes();
			}
			else
			{
				$set['errors'] = $taxDetailModel->getErrors();
				$set['data'] = $taxDetailModel->getAttributes();
			}
			$return['taxDetail'][] = $set;
		}
		
		
		
		$noTaxDetail = $data->noTaxDetail;
		foreach( $noTaxDetail as $row )
		{
			if( (int)$row->id > 0 )
			{
				$noTaxDetailModel = NoTaxDetailModel::model()->findByPk( $row->id );
				$attr = (array)$row;
				unset( $attr['id'] );
				$attr['update_date'] = date('Y-m-d H:i:s');
			}
			else
			{
				$noTaxDetailModel = new NoTaxDetailModel();
				$attr = (array)$row;
				unset( $attr['id'] );
				$attr['create_date'] = date('Y-m-d H:i:s');
				$attr['update_date'] = date('Y-m-d H:i:s');
			}
			
			
			$noTaxDetailModel->attributes = $attr;
			
			$set = array();
			$set['result'] = $noTaxDetailModel->save();
			if( $set['result'] === true )
			{
				$set['errors'] = array();
				$getModel = NoTaxDetailModel::model()->findByPk($noTaxDetailModel->id);
				$set['data'] = $getModel->getAttributes();
			}
			else
			{
				$set['errors'] = $noTaxDetailModel->getErrors();
				$set['data'] = $noTaxDetailModel->getAttributes();
			}
			$return['noTaxDetail'][] = $set;
		}
		
		
		echo json_encode($return);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 明細行削除
	 */
	public function actionDetailRemove()
	{
		$target = Input::GetPost('target');
		$id = Input::GetPost('id');
		$model = $target::model()->findByPk( $id );
		echo json_encode($model->delete());
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param MatterModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='matter-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
}
