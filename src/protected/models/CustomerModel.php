<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property string $id
 * @property string $company_name
 * @property string $company_honorific
 * @property string $name1
 * @property string $name2
 * @property string $honorific
 * @property string $kana1
 * @property string $kana2
 * @property string $zip
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $tel1
 * @property string $tel2
 * @property string $fax
 * @property string $url
 * @property string $email1
 * @property string $email2
 * @property string $remark
 * @property string $create_date
 * @property string $update_date
 */
class CustomerModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name1, name2, honorific, kana1, kana2, create_date, update_date', 'required'),
			array('company_name, company_honorific, pref, name1, name2, honorific, kana1, kana2, address1, address2, address3, fax, url, email1, email2', 'length', 'max'=>255),
			array('zip, tel1, tel2', 'length', 'max'=>45),
			array('remark', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_name, company_honorific, name1, name2, honorific, kana1, kana2, zip, address1, address2, address3, tel1, tel2, fax, url, email1, email2, remark, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * リレーション設定
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			
			//案件
			'matter' => array(
				self::HAS_MANY,
				'MatterModel',
				array( 'customer_id' => 'id' ),
				'order' => 'create_date DESC'
			),
			
			
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_name' => '企業名',
			'company_honorific' => '企業名敬称',
			'name1' => '姓',
			'name2' => '名',
			'honorific' => '敬称',
			'kana1' => 'かな（姓）',
			'kana2' => 'かな（名）',
			'zip' => '郵便番号',
			'address1' => '住所1',
			'address2' => '住所2',
			'address3' => 'Address3',
			'tel1' => '電話番号（代表）',
			'tel2' => '電話番号2',
			'fax' => 'FAX番号',
			'url' => 'URL',
			'email1' => 'メールアドレス(代表)',
			'email2' => 'Email2',
			'remark' => '備考',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'pref' => '都道府県'
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 検索モデル
	 * @return CActiveDataProvider the data provider that can return the models
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('company_name',$this->company_name,true);
		$criteria->compare('company_honorific',$this->company_honorific,true);
		$criteria->compare('name1',$this->name1,true);
		$criteria->compare('name2',$this->name2,true);
		$criteria->compare('honorific',$this->honorific,true);
		$criteria->compare('kana1',$this->kana1,true);
		$criteria->compare('kana2',$this->kana2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('tel1',$this->tel1,true);
		$criteria->compare('tel2',$this->tel2,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
}
