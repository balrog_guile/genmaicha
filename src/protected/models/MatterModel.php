<?php

/**
 * This is the model class for table "matter".
 *
 * The followings are the available columns in table 'matter':
 * @property string $id
 * @property string $customer_id
 * @property string $name
 * @property string $create_date
 * @property string $update_date
 * @property string $est_date
 * @property string $bill_date
 * @property string $receipt_date
 * @property string $status
 * @property integer $tax_subtotal
 * @property integer $tax_discount
 * @property integer $notax_subtotal
 * @property integer $notax_discount
 * @property integer $tax
 * @property integer $grand_total
 * @property integer $advanced_pay
 * @property integer $payment_total
 * @property string $payment_status
 */
class MatterModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customer_id, name, create_date, update_date', 'required'),
			array('tax_subtotal, tax_discount, notax_subtotal, notax_discount, tax, tax_rate,  grand_total, advanced_pay, payment_total', 'numerical', 'integerOnly'=>true),
			array('customer_id, status, payment_status', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
			array('est_remark, bill_remark, receipt_remark, delivery_document_remark, est_date, bill_date, receipt_date, is_tax_calc, is_total_calc, delivery_document_date, tax_mode', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, customer_id, name, create_date, update_date, est_date, bill_date, receipt_date, status, tax_subtotal, tax_discount, notax_subtotal, notax_discount, tax, grand_total, advanced_pay, payment_total, payment_status', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			
			//顧客
			'customer' => array(
				self::HAS_ONE,
				'CustomerModel',
				array( 'id' => 'customer_id' )
			),
			
			//課税項目
			'taxDetail' => array(
				self::HAS_MANY,
				'TaxDetailModel',
				array(
					'matter_id' => 'id'
				),
				'order' => 'rank ASC'
			),
			
			
			//非課税項目
			'noTaxDetail' => array(
				self::HAS_MANY,
				'NoTaxDetailModel',
				array(
					'matter_id' => 'id'
				),
				'order' => 'rank ASC'
			),
			
			//支払いステータス
			'paymentStatusMaster' => array(
				self::HAS_ONE,
				'PaymentStatusMasterModel',
				array( 'id' => 'payment_status' )
			),
			
			//案件ステータス
			'statusMaster' => array(
				self::HAS_ONE,
				'MatterStatusMasterModel',
				array( 'id' => 'status' )
			),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => '顧客ID',
			'name' => '案件名',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'est_date' => '見積書日付',
			'bill_date' => '請求書日付',
			'receipt_date' => '領収書日付',
			'status' => '案件ステータス',
			'tax_subtotal' => '課税項目合計',
			'tax_discount' => '課税項目値引き',
			'notax_subtotal' => '非課税項目合計',
			'notax_discount' => '非課税項目割引',
			'tax' => '消費税額',
			'grand_total' => '総合計額',
			'advanced_pay' => '前受金',
			'payment_total' => '支払い総合計',
			'payment_status' => '支払いステータス',
			'is_tax_calc' => '消費税計算をするか',
			'is_total_calc' => '合計計算をするか',
			'tax_rate' => '税率',
			'tax_mode' => '税計算のモード',
			'delivery_document_date' => '納品書日付',
			'est_remark' => '見積書備考',
			'bill_remark' => '請求書備考',
			'receipt_remark' => '領収書備考',
			'delivery_document_remark' => '納品書備考',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('customer_id',$this->customer_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('est_date',$this->est_date,true);
		$criteria->compare('bill_date',$this->bill_date,true);
		$criteria->compare('receipt_date',$this->receipt_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('tax_subtotal',$this->tax_subtotal);
		$criteria->compare('tax_discount',$this->tax_discount);
		$criteria->compare('notax_subtotal',$this->notax_subtotal);
		$criteria->compare('notax_discount',$this->notax_discount);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('grand_total',$this->grand_total);
		$criteria->compare('advanced_pay',$this->advanced_pay);
		$criteria->compare('payment_total',$this->payment_total);
		$criteria->compare('payment_status',$this->payment_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MatterModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
