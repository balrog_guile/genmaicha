<?php

/**
 * This is the model class for table "no_tax_detail".
 *
 * The followings are the available columns in table 'no_tax_detail':
 * @property string $id
 * @property string $matter_id
 * @property string $name
 * @property string $remark
 * @property integer $price
 * @property string $qty
 * @property string $qty_unit
 * @property integer $sub_total
 * @property string $rank
 * @property string $create_date
 * @property string $update_date
 */
class NoTaxDetailModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'no_tax_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('matter_id, name, create_date, update_date', 'required'),
			array('price, sub_total', 'numerical', 'integerOnly'=>true),
			array('matter_id, qty, rank', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
			array('qty_unit', 'length', 'max'=>45),
			array('remark, targeting', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, matter_id, name, remark, price, qty, qty_unit, sub_total, rank, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'matter_id' => '案件ID',
			'name' => '項目名',
			'remark' => '備考',
			'price' => '項目単価',
			'qty' => '項目数量',
			'qty_unit' => '項目数量単位',
			'sub_total' => '項目合計',
			'rank' => '表示順',
			'create_date' => '作成日',
			'update_date' => '編集日',
			'targeting' => '請求項目',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('matter_id',$this->matter_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('qty',$this->qty,true);
		$criteria->compare('qty_unit',$this->qty_unit,true);
		$criteria->compare('sub_total',$this->sub_total);
		$criteria->compare('rank',$this->rank,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NoTaxDetailModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
