<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */
/* @var $form CActiveForm */
?>

<div class="">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	
	<hr />
	<p class="note"><span class="required">*</span>は必須</p>
	<hr />
	
	<?php echo $form->errorSummary($model); ?>
	
	
	<div class="row">
		<div class="col-md-8">
			<?php echo $form->labelEx($model,'company_name'); ?>
			<?php echo $form->textField($model,'company_name',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'company_name'); ?>
		</div>
		
		<div class="col-md-4">
			<?php echo $form->labelEx($model,'company_honorific'); ?>
			<?php echo $form->textField($model,'company_honorific',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'company_honorific'); ?>
		</div>
	</div>
	
	
	<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'name1'); ?>
				<?php echo $form->textField($model,'name1',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'name1'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'name2'); ?>
				<?php echo $form->textField($model,'name2',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'name2'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'honorific'); ?>
				<?php echo $form->textField($model,'honorific',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'honorific'); ?>
			</div>
		</div>


		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'kana1'); ?>
				<?php echo $form->textField($model,'kana1',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'kana1'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'kana2'); ?>
				<?php echo $form->textField($model,'kana2',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'kana2'); ?>
			</div>
		</div>
	
	
	
	
	<ul class="nav nav-tabs bottom15 top15">
		<li role="presentation" class="active"><a href="#cutomerInfo">お客様情報</a></li>
		<li role="presentation"><a href="#matterArea">案件(最新10件)</a></li>
	</ul>
	
	
	
	
	<!--  お客様情報 ------------------------------------------------------------>
	<div class="tab-panels" id="cutomerInfo">
		
		<div class="row">
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'pref'); ?>
				<?php
					echo $form->dropDownList(
						$model,
						'pref',
						UtilityHelper::getPrefList(true),
						array('class' => 'form-control')
					);
				?>
				<?php echo $form->error($model,'pref'); ?>
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'zip'); ?>
				<?php echo $form->textField($model,'zip',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'zip'); ?>
			</div>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'address1'); ?>
			<?php echo $form->textField($model,'address1',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'address1'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'address2'); ?>
			<?php echo $form->textField($model,'address2',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'address2'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'address3'); ?>
			<?php echo $form->textField($model,'address3',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'address3'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'tel1'); ?>
			<?php echo $form->textField($model,'tel1',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'tel1'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'tel2'); ?>
			<?php echo $form->textField($model,'tel2',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'tel2'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'fax'); ?>
			<?php echo $form->textField($model,'fax',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'fax'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'url'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'email1'); ?>
			<?php echo $form->textField($model,'email1',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'email1'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'email2'); ?>
			<?php echo $form->textField($model,'email2',array('class' => 'form-control')); ?>
			<?php echo $form->error($model,'email2'); ?>
		</div>

		<div class="">
			<?php echo $form->labelEx($model,'remark'); ?>
			<?php echo $form->textArea($model,'remark',array('class' => 'form-control', 'rows' => 10)); ?>
			<?php echo $form->error($model,'remark'); ?>
		</div>
	</div>
	<!--  /お客様情報 ------------------------------------------------------------>
	
	
	
	<!-- 案件情報 -------------------------------------------------------------->
	<div class="tab-panels" id="matterArea">
		
		<?php if( $model->isNewRecord ): ?>
			<div class="alert alert-info" role="alert">
				案件情報閲覧／案件情報作成は、顧客情報登録完了後行うことができます。
			</div>
		<?php else: ?>
			<?php
				echo CHtml::link(
					'案件作成',
					Yii::app()->createUrl('matter/create',array('cid'=>$model->id)),
					array('class' => 'btn btn-success')
				);
			?>
		<?php
				echo CHtml::link(
					'全件検索',
					'',
					array('class' => 'btn btn-success')
				);
			?>
			<hr />
			
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>案件名</th>
						<th>作成日</th>
						<th>合計金額</th>
						<th>ステータス</th>
						<th>支払いステータス</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach( $model->matter as $matter ): ?>
						<tr>
							<td><?php echo $matter->name; ?></td>
							<td><?php echo $matter->create_date; ?></td>
							<td>
								<?php echo number_format($matter->grand_total); ?>円</td>
							</td>
							<td>
								<?php if(is_null($matter->statusMaster)): ?>
									未設定
								<?php else: ?>
									<?php echo $matter->statusMaster->name; ?>
								<?php endif; ?>
							</td>
							<td>
								<?php if(is_null($matter->paymentStatusMaster)): ?>
									未設定
								<?php else: ?>
									<?php echo $matter->paymentStatusMaster->name; ?>
								<?php endif; ?>
							</td>
							<td>
								<?php
									echo CHtml::link(
										'詳細', 
										Yii::app()->createUrl('matter/update', array('id' => $matter->id ) ),
										array(
											'class' => 'btn btn-primary'
										)
									);
								?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>
	<!-- /案件情報 -------------------------------------------------------------->
	
	
	
	<hr />
	
	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '登録' : '編集',array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->