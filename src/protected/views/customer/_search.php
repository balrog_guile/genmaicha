<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */
/* @var $form CActiveForm */
?>

<div class="wide">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="">
		<?php echo $form->label($model,'company_name'); ?>
		<?php echo $form->textField($model,'company_name',array('class' => 'form-control')); ?>
	</div>
	
	<div class="">
		<div class="col-md-6">
			<?php echo $form->label($model,'name1'); ?>
			<?php echo $form->textField($model,'name1',array('class' => 'form-control')); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->label($model,'name2'); ?>
			<?php echo $form->textField($model,'name2',array('class' => 'form-control')); ?>
		</div>
	</div>
	
	<div class="">
		<div class="col-md-6">
			<?php echo $form->label($model,'kana1'); ?>
			<?php echo $form->textField($model,'kana1',array('class' => 'form-control')); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->label($model,'kana2'); ?>
			<?php echo $form->textField($model,'kana2',array('class' => 'form-control')); ?>
		</div>
	</div>
	
	<div class="">
		<div class="col-md-6">
			<?php echo $form->label($model,'tel1'); ?>
			<?php echo $form->textField($model,'tel1',array('class' => 'form-control')); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->label($model,'tel2'); ?>
			<?php echo $form->textField($model,'tel2',array('class' => 'form-control')); ?>
		</div>
	</div>
	<div class="">
		<?php echo $form->label($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('class' => 'form-control')); ?>
	</div>
	
	<div class="">
		<?php echo $form->label($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('class' => 'form-control')); ?>
	</div>
	
	<div class="">
		<?php echo $form->label($model,'email1'); ?>
		<?php echo $form->textField($model,'email1',array('class' => 'form-control')); ?>
	</div>
	
	<div class="">
		<?php echo $form->label($model,'email2'); ?>
		<?php echo $form->textField($model,'email2',array('class' => 'form-control')); ?>
	</div>
	
	<div class="buttons">
		<?php echo CHtml::submitButton('検索',array('class' => 'btn btn-success')); ?>
		<?php echo CHtml::link( '検索解除', Yii::app()->createUrl('customer/admin'), array('class' => 'btn btn-danger') ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->