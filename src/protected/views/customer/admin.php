<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */

$this->breadcrumbs=array(
	'Customer Models'=>array('index'),
	'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#customer-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>顧客管理</h1>

<hr />
<?php echo CHtml::link( '新規顧客情報作成', $this->createUrl('create'), array('class' => 'btn btn-primary' ) ); ?>
<hr />

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>

<hr />

<?php $dataProvider = $model->search(); ?>


<?php foreach( $dataProvider->getData() as $data ):?>
	<table class="table table-striped table-bordered">
		<tr>
			<th>企業名<br />顧客名</th>
			<th>住所</th>
			<th>電話<br />FAX</th>
			<th>URL/Eメール</th>
			<th>備考</th>
			<th>操作</th>
		</tr>
		<tr>
			<td>
				<?php echo $data->company_name; ?><?php echo $data->company_honorific; ?><br/>
				<?php echo $data->name1; ?> <?php echo $data->name2; ?> <?php echo $data->honorific; ?><br />
				<?php echo $data->kana1; ?> <?php echo $data->kana2; ?> <?php echo $data->honorific; ?>
			</td>
			<td>
				<?php echo $data->zip; ?><br />
				<?php echo $data->pref; ?><br />
				<?php echo $data->address1; ?><br />
				<?php echo $data->address2; ?><br />
				<?php echo $data->address3; ?>
			</td>
			<td>
				<?php echo $data->tel1; ?><br />
				<?php echo $data->tel2; ?><br />
				<?php echo $data->fax; ?>
			</td>
			<td>
				<?php echo $data->url; ?><br />
				<?php echo $data->email1; ?><br />
				<?php echo $data->email2; ?>
			</td>
			<td>
				<?php echo $data->remark; ?>
			</td>

			<td class="alingCenter">
				<?php echo CHtml::link(
						'詳細／編集',
						Yii::app()->createUrl('customer/update',array('id'=>$data->id)),
						array(
							'class' => 'btn btn-primary'
							)
					);
				?><br />
				<?php
					echo CHtml::link(
						'案件作成',
						Yii::app()->createUrl('matter/create',array('cid'=>$data->id)),
						array('class' => 'btn btn-primary')
					);
				?>
			</td>
		</tr>
		<tr>
			<th colspan="6">案件</th>
		</tr>
		<tr>
			<td colspan="6">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>案件名</th>
							<th>作成日</th>
							<th>合計金額</th>
							<th>ステータス</th>
							<th>支払いステータス</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach( $data->matter as $matter ): ?>
							<tr>
								<td><?php echo $matter->name; ?></td>
								<td><?php echo $matter->create_date; ?></td>
								<td>
									<?php echo number_format($matter->grand_total); ?>円</td>
								</td>
								<td>
									<?php if(is_null($matter->statusMaster)): ?>
										未設定
									<?php else: ?>
										<?php echo $matter->statusMaster->name; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php if(is_null($matter->paymentStatusMaster)): ?>
										未設定
									<?php else: ?>
										<?php echo $matter->paymentStatusMaster->name; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php
										echo CHtml::link(
											'詳細', 
											Yii::app()->createUrl('matter/update', array('id' => $matter->id ) ),
											array(
												'class' => 'btn btn-primary'
											)
										);
									?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	
	<hr />
<?php endforeach; ?>


<hr />
<?php
$this->widget(
	'CLinkPager',
	array(
		'pages' => $dataProvider->getPagination(),
	)
);?>
