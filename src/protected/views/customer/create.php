<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */

$this->breadcrumbs=array(
	'Customer Models'=>array('index'),
	'Create',
);

$this->menu=array();
?>

<h1>顧客情報作成</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>