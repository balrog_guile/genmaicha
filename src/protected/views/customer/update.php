<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */

$this->breadcrumbs=array();

$this->menu=array();
?>

<h1>顧客情報編集</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>