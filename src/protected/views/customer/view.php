<?php
/* @var $this CustomerController */
/* @var $model CustomerModel */

$this->breadcrumbs=array(
	'Customer Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustomerModel', 'url'=>array('index')),
	array('label'=>'Create CustomerModel', 'url'=>array('create')),
	array('label'=>'Update CustomerModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustomerModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustomerModel', 'url'=>array('admin')),
);
?>

<h1>View CustomerModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'company_name',
		'company_honorific',
		'name1',
		'name2',
		'honorific',
		'kana1',
		'kana2',
		'zip',
		'address1',
		'address2',
		'address3',
		'tel1',
		'tel2',
		'fax',
		'url',
		'email1',
		'email2',
		'remark',
		'create_date',
		'update_date',
	),
)); ?>
