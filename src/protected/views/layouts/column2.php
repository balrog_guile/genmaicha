<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<!-- サイド -->
<?php if( count($this->menu) > 0 ): ?>
	<div class="row">
		<div class="col-md-12">
			<div id="sidebar">
				<?php
					$this->beginWidget('zii.widgets.CPortlet', array(
						'title'=>'',
					));
					$this->widget('zii.widgets.CMenu', array(
						'items'=>$this->menu,
						'htmlOptions'=>array('class'=>'operations'),
					));
					$this->endWidget();
				?>
			</div>
		</div>
	</div>
<?php endif; ?>
<!-- /サイド -->



<!-- メイン -->
<div class="row">
	<div class="col-md-12">
		<div id="content">
		<?php echo $content; ?>
		</div>
	</div>
</div>
<!-- /メイン -->

<?php $this->endContent(); ?>