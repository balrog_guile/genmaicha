<?php
$cs        = Yii::app()->clientScript;
$themePath = Yii::app()->theme->baseUrl;

/**
 * StyleSHeets
 */
// テーマ
// $cs->registerCssFile($themePath . '/assets/css/bootstrap-theme.css');

/**
 * JavaScripts
 */
$cs->registerCoreScript('jquery', CClientScript::POS_END);
$cs->registerCoreScript('jquery.ui', CClientScript::POS_END);
$cs->registerScriptFile($themePath . '/assets/js/bootstrap.min.js', CClientScript::POS_END);
$cs->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();$('[data-toggle=\"popover\"]').tooltip()", CClientScript::POS_READY);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
   <meta charset="utf-8">
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
       <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
       <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
   <![endif]-->
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
   <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
    <div class="container">
        <?php $this->widget('bootstrap.widgets.BsNavbar', array(
            'collapse' => true,
            'brandLabel' => CHtml::encode(Yii::app()->name),
            'brandUrl' => Yii::app()->homeUrl,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',
                    'activateParents' => true,
                    'items' => array(
                        array(
                            'label' => 'Home',
                            'url' => array(
                                '/site/index',
                            )
                        ),
						/*
                        array(
                            'label' => 'About',
                            'url' => array(
                                '/site/page',
                                'view' => 'about'
                            )
                        ),
                        array(
                            'label' => 'Contact',
                            'url' => array(
                                '/site/contact'
                            )
                        ),
						 */
                        array(
                            'label' => 'Login',
                            'url' => array(
                                '/site/login'
                            ),
                            'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT,
                            'visible' => Yii::app()->user->isGuest
                        ),
                        array(
                            'label' => 'Logout (' . Yii::app()->user->name . ')',
                            'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT,
                            'url' => array(
                                '/site/logout'
                            ),
                            'visible' => !Yii::app()->user->isGuest
                        )
                    ),
                    'htmlOptions' => array(
                        'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT
                    )
                )
            )
        ));
        ?>
		
		<?php echo $content; ?>
		
		
		<div class="navbar navbar-default footer-area" role="navigation">
			作成監修：春芳堂
		</div>
	</div>
</body>
</html>