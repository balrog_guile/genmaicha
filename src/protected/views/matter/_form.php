
<!--
KOのテスト
<table class="table table-bordered table-striped">
	<tbody data-bind="foreach: test">
        <tr>
            <td>
				<input type="text" name="one" value="" data-bind="value: firstName">
			</td>
            <td data-bind="text: lastName"></td>
        </tr>
    </tbody>
</table>
<a href="javascript:void(0)" class="test">テスト</a>
-->


<table class="table table-bordered table-striped">
	<tr>
		<th>対象顧客</th>
		<th>企業</th>
		<td>
			<?php echo $customer->company_name; ?>
			<?php echo $customer->company_honorific; ?><br />
		</td>
		<th>顧客</th>
		<td>
			<?php echo $customer->name1; ?>
			<?php echo $customer->name2; ?>
			<?php echo $customer->honorific; ?>
		</td>
		<td>
			<?php echo CHtml::link( '顧客情報', '#', array('class' => 'btn btn-default') ); ?>
		</td>
	</tr>
</table>

<hr />

<?php if( $model->isNewRecord === TRUE ): ?>
	<div class="alert alert-info" role="alert">
		明細項目は、基本情報登録後に入力可能です
	</div>
<?php endif; ?>



<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'matter-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	
	
	<!-- 隠し項目 -->
	<?php echo $form->hiddenField($model,'customer_id'); ?>
	<!-- /隠し項目 -->
	
	
	<?php echo $form->errorSummary($model); ?>
	<div class="">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	
	<hr />
	
	
	<!-- 各種日付 -->
	<?php if( $model->isNewRecord !== TRUE ): ?>
		<div class="row">
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'est_date'); ?>
				<?php echo $form->textField($model,'est_date',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'est_date'); ?>
				
				<br />
				<?php echo $form->labelEx($model,'est_remark'); ?>
				<?php echo $form->textArea($model,'est_remark',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'est_remark'); ?>
				
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'bill_date'); ?>
				<?php echo $form->textField($model,'bill_date',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'bill_date'); ?>
				
				<br />
				<?php echo $form->labelEx($model,'bill_remark'); ?>
				<?php echo $form->textArea($model,'bill_remark',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'bill_remark'); ?>
				
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'receipt_date'); ?>
				<?php echo $form->textField($model,'receipt_date',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'receipt_date'); ?>
				
				<br />
				<?php echo $form->labelEx($model,'receipt_remark'); ?>
				<?php echo $form->textArea($model,'receipt_remark',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'receipt_remark'); ?>
				
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'delivery_document_date'); ?>
				<?php echo $form->textField($model,'delivery_document_date',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'delivery_document_date'); ?>
				<br />
				<?php echo $form->labelEx($model,'delivery_document_remark'); ?>
				<?php echo $form->textArea($model,'delivery_document_remark',array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'delivery_document_remark'); ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- /各種日付 -->
	
	
	
	<hr />
	
	
	
	<!-- 各種ステータス -->
	<div class="row">
		<div class="col-md-6">
			<?php echo $form->labelEx($model,'status'); ?>
			<?php
				echo $form->dropDownList(
					$model, 'status', UtilityHelper::getStatusList(TRUE),
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'payment_status'); ?>
		</div>
		<div class="col-md-6">
			<?php echo $form->labelEx($model,'payment_status'); ?>
			<?php
				echo $form->dropDownList(
					$model, 'payment_status', UtilityHelper::getPaymentStatusList(TRUE),
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'payment_status'); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'is_tax_calc'); ?>
			<?php
				echo $form->dropDownList(
					$model, 'is_tax_calc', array( '1' => 'する', '0' => 'しない'),
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'is_tax_calc'); ?>
		</div>
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'is_total_calc'); ?>
			<?php
				echo $form->dropDownList(
					$model, 'is_total_calc', array( '1' => 'する', '0' => 'しない'),
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'is_total_calc'); ?>
		</div>
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'tax_rate'); ?>
			<?php
				echo $form->textField(
					$model, 'tax_rate',
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'tax_rate'); ?>
		</div>
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'tax_mode'); ?>
			<?php
				echo $form->dropDownList(
					$model, 'tax_mode',
					array( '1' => '切り捨て', '2' => '切り上げ', '3' => '四捨五入'),
					array( 'class' => 'form-control' )
				);
			?>
			<?php echo $form->error($model,'tax_mode'); ?>
		</div>
	</div>
	<!-- /各種ステータス -->
	
	
	
	<hr />
	<div class="buttons">
		<?php
			echo CHtml::submitButton(
				($model->isNewRecord ? '作成実行' : '基本情報の編集実行'),
				array( 'class' => 'btn btn-primary form-control' )
			);
		?>
	</div>
	
	
	
	<hr />
	
	
	<!-- 各種書類発行 -->
	<?php if( $model->isNewRecord !== TRUE ): ?>
		<div class="row">
			<div class="col-md-3">
				<?php
					echo CHtml::link(
						'見積書作成',
						'#',
						array(
							'target' => '_blank',
							'class' => 'btn btn-success form-control',
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php
					echo CHtml::link(
						'請求書作成',
						Yii::app()->createUrl('matter/document',array('mode' => 'bill', 'id' => $model->id )),
						array(
							'target' => '_blank',
							'class' => 'btn btn-success form-control',
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php
					echo CHtml::link(
						'領収書作成',
						'#',
						array(
							'target' => '_blank',
							'class' => 'btn btn-success form-control',
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php
					echo CHtml::link(
						'納品書作成',
						'#',
						array(
							'target' => '_blank',
							'class' => 'btn btn-success form-control',
						)
					);
				?>
			</div>
		</div>
		<hr />
	<?php endif; ?>
	<!-- /各種書類発行 -->
	
	
	<!-- 明細項目 -->
	<?php if( $model->isNewRecord !== TRUE ): ?>
		
		<h2>勘定明細</h2>
		
		<!-- 課税項目 ----------------------------------------------->
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th colspan="8">
						課税項目明細
						<!--
						<?php echo CHtml::button(
								'明細追加',
								array(
									'class' => 'btn btn-sm btn-primary',
									'data-bind' => 'click: addDetailTax',
								)
							);
						?>
						-->
					</th>
				</tr>
				<tr>
					<th>操作</th>
					<th>請求対象</th>
					<th>摘要</th>
					<th>備考</th>
					<th>単価</th>
					<th>数量</th>
					<th>単位</th>
					<th>合計</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: taxDetailRows" class="taxDetail">
				<tr>
					<td>
						<!--
						<span data-bind="if:id">
							<?php
								echo CHtml::button(
									'アップデート',
									array(
										'class' => 'btn btn-xs btn-primary',
										//'data-bind' => 'click: setRowDetailTax'
									)
								);
							?>
						</span>
						<span data-bind="ifnot:id">
							<?php
								echo CHtml::button(
									'　　登録　　',
									array(
										'class' => 'btn btn-xs btn-primary',
										//'data-bind' => 'click: setRowDetailTax'
									)
								);
							?>
						</span>
						<br />
						-->
						<?php
							echo CHtml::button(
								'削除',
								array(
									'class' => 'btn btn-xs btn-danger',
									'data-bind' => 'click: removeThis',
								)
							);
						?><br />
						<span data-bind="ifnot:id" class="label label-warning">保存されていません</span>
						
						<span data-bind="if:isError" class="label label-warning">エラーあり。保存されていません</span>
						<div data-bind="foreach: errors">
							<span data-bind="text:$data" class="label label-warning">エラー</span>
						</div>
						<hr />
						<?php echo CHtml::button( '▲', array( 'class' => 'btn btn-default btn-xs', 'data-bind' => 'click:rankChange;' ) ); ?>
						<?php echo CHtml::button( '▼', array( 'class' => 'btn btn-default btn-xs', 'data-bind' => 'click:rankChange;') ); ?>
					</td>
					<td>
						<?php
							echo Chtml::checkBox(
								'c',
								false,
								array(
									'value' => 1,
									'data-bind' => 'checked: targeting'
								)
							);
						?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: name',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: remark',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: price;',
							'class' => 'form-control',
						) ); ?>円
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: qty;',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: qty_unit',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: sub_total',
							'class' => 'form-control',
							'readonly' => 'readonly',
						) ); ?>円
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="8">
						<?php echo CHtml::button(
								'明細追加',
								array(
									'class' => 'btn btn-sm btn-primary',
									'data-bind' => 'click: taxRowAdd',
								)
							);
						?>
					</th>
				</tr>
				<tr>
					<th colspan="7">
						<?php echo $form->labelEx($model,'tax_discount'); ?>
					</th>
					<td>
						<?php
							echo CHtml::textField(
								'c', '', 
								array(
									'class' => 'form-control',
									'data-bind' => 'value: matter.tax_discount',
								)
							);
						?>
						円
					</td>
				</tr>
				<tr>
					<th colspan="7">
						<?php echo $form->labelEx($model,'tax_subtotal'); ?>
					</th>
					<td>
						<?php
							echo CHtml::textField(
								'c', '', 
								array(
									'class' => 'form-control',
									'data-bind' => 'value: matter.tax_subtotal',
									'readonly' => 'readonly'
								)
							);
						?>
						円
					</td>
				</tr>
			</tfoot>
		</table>
		<!-- /課税項目 -->
		
		
		
		<!-- 非課税項目 ----------------------------------------------->
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th colspan="8">
						非課税項目明細
					</th>
				</tr>
				<tr>
					<th>操作</th>
					<th>請求対象</th>
					<th>摘要</th>
					<th>備考</th>
					<th>単価</th>
					<th>数量</th>
					<th>単位</th>
					<th>合計</th>
				</tr>
			</thead>
			<tbody data-bind="foreach: noTaxDetailRows" class="taxDetail">
				<tr>
					<td>
						<!--
						<span data-bind="if:id">
							<?php
								echo CHtml::button(
									'アップデート',
									array(
										'class' => 'btn btn-xs btn-primary',
										//'data-bind' => 'click: setRowDetailTax'
									)
								);
							?>
						</span>
						<span data-bind="ifnot:id">
							<?php
								echo CHtml::button(
									'　　登録　　',
									array(
										'class' => 'btn btn-xs btn-primary',
										//'data-bind' => 'click: setRowDetailTax'
									)
								);
							?>
						</span>
						<br />
						-->
						<?php
							echo CHtml::button(
								'削除',
								array(
									'class' => 'btn btn-xs btn-danger',
									'data-bind' => 'click: removeThis',
								)
							);
						?><br />
						<span data-bind="ifnot:id" class="label label-warning">保存されていません</span>
						
						<span data-bind="if:isError" class="label label-warning">エラーあり。保存されていません</span>
						<div data-bind="foreach: errors">
							<span data-bind="text:$data" class="label label-warning">エラー</span>
						</div>
						<hr />
						<!--<span data-bind="text: rank"></span>-->
						<?php echo CHtml::button( '▲', array( 'class' => 'btn btn-default btn-xs', 'data-bind' => 'click:rankChange;' ) ); ?>
						<?php echo CHtml::button( '▼', array( 'class' => 'btn btn-default btn-xs', 'data-bind' => 'click:rankChange;') ); ?>
					</td>
					<td>
						<?php
							echo Chtml::checkBox(
								'c',
								false,
								array(
									'value' => 1,
									'data-bind' => 'checked: targeting'
								)
							);
						?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: name',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: remark',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: price;',
							'class' => 'form-control',
						) ); ?>円
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: qty;',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: qty_unit',
							'class' => 'form-control',
						) ); ?>
					</td>
					<td>
						<?php echo CHtml::textField( 'c', '', array(
							'data-bind' => 'value: sub_total',
							'class' => 'form-control',
							'readonly' => 'readonly',
						) ); ?>円
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="8">
						<?php echo CHtml::button(
								'明細追加',
								array(
									'class' => 'btn btn-sm btn-primary',
									'data-bind' => 'click: noTaxRowAdd',
								)
							);
						?>
					</th>
				</tr>
				<tr>
					<th colspan="7">
						<?php echo $form->labelEx($model,'notax_discount'); ?>
					</th>
					<td>
						<?php
							echo CHtml::textField(
								'c', '', 
								array(
									'class' => 'form-control',
									'data-bind' => 'value: matter.notax_discount',
								)
							);
						?>
						円
					</td>
				</tr>
				<tr>
					<th colspan="7">
						<?php echo $form->labelEx($model,'notax_subtotal'); ?>
					</th>
					<td>
						<?php
							echo CHtml::textField(
								'c', '', 
								array(
									'class' => 'form-control',
									'data-bind' => 'value: matter.notax_subtotal',
									'readonly' => 'readonly'
								)
							);
						?>
						円
					</td>
				</tr>
			</tfoot>
		</table>
		<!-- /課税項目 -->
		
		
		
		
		<hr />
		
	<?php endif; ?>
	<!-- /明細項目 -->
	
	
	<!-- 合計項目など -->
	<?php if( $model->isNewRecord !== TRUE ): ?>
		
		<div class="row">
		
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'advanced_pay'); ?>
				<?php
					echo CHtml::textField(
						'c', '', 
						array(
							'class' => 'form-control',
							'data-bind' => 'value: matter.advanced_pay',
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'tax'); ?>
				<?php
					echo CHtml::textField(
						'c', '', 
						array(
							'class' => 'form-control',
							'data-bind' => 'value: matter.tax',
							'readonly' => 'readonly'
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'grand_total'); ?>
				<?php
					echo CHtml::textField(
						'c', '', 
						array(
							'class' => 'form-control',
							'data-bind' => 'value: matter.grand_total',
							'readonly' => 'readonly'
						)
					);
				?>
			</div>
			<div class="col-md-3">
				<?php echo $form->labelEx($model,'payment_total'); ?>
				<?php
					echo CHtml::textField(
						'c', '', 
						array(
							'class' => 'form-control',
							'data-bind' => 'value: matter.payment_total',
							'readonly' => 'readonly'
						)
					);
				?>
			</div>
		</div>
		
		<hr />
	<?php endif; ?>
	<!-- /合計項目など -->
	
	
	
	

<?php $this->endWidget(); ?>

</div><!-- form -->