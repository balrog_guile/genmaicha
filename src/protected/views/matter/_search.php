<?php
/* @var $this MatterController */
/* @var $model MatterModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'est_date'); ?>
		<?php echo $form->textField($model,'est_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_date'); ?>
		<?php echo $form->textField($model,'bill_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receipt_date'); ?>
		<?php echo $form->textField($model,'receipt_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tax_subtotal'); ?>
		<?php echo $form->textField($model,'tax_subtotal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tax_discount'); ?>
		<?php echo $form->textField($model,'tax_discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notax_subtotal'); ?>
		<?php echo $form->textField($model,'notax_subtotal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notax_discount'); ?>
		<?php echo $form->textField($model,'notax_discount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tax'); ?>
		<?php echo $form->textField($model,'tax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grand_total'); ?>
		<?php echo $form->textField($model,'grand_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'advanced_pay'); ?>
		<?php echo $form->textField($model,'advanced_pay'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payment_total'); ?>
		<?php echo $form->textField($model,'payment_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payment_status'); ?>
		<?php echo $form->textField($model,'payment_status',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->