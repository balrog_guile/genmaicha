<?php
/* @var $this MatterController */
/* @var $data MatterModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('est_date')); ?>:</b>
	<?php echo CHtml::encode($data->est_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bill_date')); ?>:</b>
	<?php echo CHtml::encode($data->bill_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt_date')); ?>:</b>
	<?php echo CHtml::encode($data->receipt_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax_subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->tax_subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax_discount')); ?>:</b>
	<?php echo CHtml::encode($data->tax_discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notax_subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->notax_subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notax_discount')); ?>:</b>
	<?php echo CHtml::encode($data->notax_discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax')); ?>:</b>
	<?php echo CHtml::encode($data->tax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grand_total')); ?>:</b>
	<?php echo CHtml::encode($data->grand_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advanced_pay')); ?>:</b>
	<?php echo CHtml::encode($data->advanced_pay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_total')); ?>:</b>
	<?php echo CHtml::encode($data->payment_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_status')); ?>:</b>
	<?php echo CHtml::encode($data->payment_status); ?>
	<br />

	*/ ?>

</div>