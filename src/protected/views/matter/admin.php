<?php
/* @var $this MatterController */
/* @var $model MatterModel */

$this->breadcrumbs=array(
	'Matter Models'=>array('index'),
	'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#matter-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>案件DB</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>



<?php $dataProvider = $model->search(); ?>


<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>作成日</th>
			<th>案件名</th>
			<th>顧客</th>
			<th>合計額</th>
			<th>案件ステータス</th>
			<th>支払いステータス</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $dataProvider->getData() as $data ):?>
			<tr>
				<td><?php echo $data->create_date; ?></td>
				<td>
					<?php echo $data->name; ?>
				</td>
				<td>
					<a href="<?php echo Yii::app()->createUrl('customer/update',array('id'=>$data->customer->id)); ?>">
						<?php echo $data->customer->company_name; ?>
						<?php echo $data->customer->company_honorific; ?>
						<br />
						<?php echo $data->customer->name1; ?>
						<?php echo $data->customer->name2; ?>
						<?php echo $data->customer->honorific; ?>
					</a>
				</td>
				<td>
					<?php echo number_format($data->grand_total); ?>円
				</td>
				<td>
					<?php if(is_null($data->statusMaster)): ?>
						未設定
					<?php else: ?>
						<?php echo $data->statusMaster->name; ?>
					<?php endif; ?>
				</td>
				<td>
					<?php if(is_null($data->paymentStatusMaster)): ?>
						未設定
					<?php else: ?>
						<?php echo $data->paymentStatusMaster->name; ?>
					<?php endif; ?>
				</td>
				<td>
					<?php
						echo CHtml::link(
							'参照/編集',
							Yii::app()->createUrl('matter/update',array('id' => $data->id)),
							array(
								'class' => 'btn btn-primary'
							)
						);
					?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>





<hr />
<?php
$this->widget(
	'CLinkPager',
	array(
		'pages' => $dataProvider->getPagination(),
	)
);?>


