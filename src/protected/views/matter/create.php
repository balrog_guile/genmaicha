<?php
/* @var $this MatterController */
/* @var $model MatterModel */

$this->breadcrumbs=array(
	'Matter Models'=>array('index'),
	'Create',
);

$this->menu=array();

?>

<div class="row">
	<div class="col-md-3">
		<h1>案件作成</h1>
	</div>
	<div class="col-md-9 matter-menu">
		<?php
			$this->widget('bootstrap.widgets.BsNavbar', array(
				'collapse' => true,
				'brandLabel' => '',
				'brandUrl' => '',
				'items' => array(
					BsHtml::menuText(
						BsHtml::link(
							'顧客へ',
							array('customer/update','id'=>$customer->id),
							array('class' => 'navbar-link')
						),
						array('pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT)
					),
					BsHtml::menuText(
						BsHtml::link(
							'案件管理へ',
							array('matter/admin'),
							array('class' => 'navbar-link')
						),
						array('pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT)
					),
				)
				));
		?>
	</div>
</div>

<hr />

<?php
	$this->renderPartial(
		'_form',
		array(
			'model'=>$model,
			'customer' => $customer
		)
	);
?>