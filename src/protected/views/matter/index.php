<?php
/* @var $this MatterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Matter Models',
);

$this->menu=array(
	array('label'=>'Create MatterModel', 'url'=>array('create')),
	array('label'=>'Manage MatterModel', 'url'=>array('admin')),
);
?>

<h1>Matter Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
