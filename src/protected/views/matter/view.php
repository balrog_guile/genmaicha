<?php
/* @var $this MatterController */
/* @var $model MatterModel */

$this->breadcrumbs=array(
	'Matter Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MatterModel', 'url'=>array('index')),
	array('label'=>'Create MatterModel', 'url'=>array('create')),
	array('label'=>'Update MatterModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MatterModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MatterModel', 'url'=>array('admin')),
);
?>

<h1>View MatterModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'name',
		'create_date',
		'update_date',
		'est_date',
		'bill_date',
		'receipt_date',
		'status',
		'tax_subtotal',
		'tax_discount',
		'notax_subtotal',
		'notax_discount',
		'tax',
		'grand_total',
		'advanced_pay',
		'payment_total',
		'payment_status',
	),
)); ?>
