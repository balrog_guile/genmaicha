<?php
/* @var $this MatterStatusMasterController */
/* @var $model MatterStatusMasterModel */
/* @var $form CActiveForm */
?>

<hr />

<div class="">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'matter-status-master-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name', array( 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'is_finishing'); ?>
		<div>
			<?php echo $form->checkBox($model, 'is_finishing', array() ); ?>
		</div>
		<?php echo $form->error($model,'is_finishing'); ?>
	</div>
	
	<hr />
	
	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '作成' : '編集', array( 'class'=>'btn btn-primary') ); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->