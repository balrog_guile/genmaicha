<?php
/* @var $this MatterStatusMasterController */
/* @var $data MatterStatusMasterModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_finishing')); ?>:</b>
	<?php echo CHtml::encode($data->is_finishing); ?>
	<br />


</div>