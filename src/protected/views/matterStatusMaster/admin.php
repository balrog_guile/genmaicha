<?php
/* @var $this MatterStatusMasterController */
/* @var $model MatterStatusMasterModel */

$this->breadcrumbs=array(
	'Matter Status Master Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	/*
	array('label'=>'List MatterStatusMasterModel', 'url'=>array('index')),
	array('label'=>'Create MatterStatusMasterModel', 'url'=>array('create')),
	*/
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#matter-status-master-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>案件ステータスマスタ</h1>


<hr />
<?php echo CHtml::link( '新規ステータス', $this->createUrl('create'), array('class' => 'btn btn-primary' ) ); ?>
<hr />


<?php /*
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
*/ ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'matter-status-master-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'is_finishing',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
