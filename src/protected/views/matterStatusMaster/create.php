<?php
/* @var $this MatterStatusMasterController */
/* @var $model MatterStatusMasterModel */

$this->breadcrumbs=array(
	'Matter Status Master Models'=>array('index'),
	'Create',
);

$this->menu=array(
	/*
	array('label'=>'List MatterStatusMasterModel', 'url'=>array('index')),
	array('label'=>'Manage MatterStatusMasterModel', 'url'=>array('admin')),
	*/
);
?>

<h1>案件ステータス作成</h1>

<?php
	$this->renderPartial(
		'_form',
		array(
			'model'=>$model
		)
	);
?>