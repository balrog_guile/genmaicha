<?php
/* @var $this MatterStatusMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Matter Status Master Models',
);

$this->menu=array(
	array('label'=>'Create MatterStatusMasterModel', 'url'=>array('create')),
	array('label'=>'Manage MatterStatusMasterModel', 'url'=>array('admin')),
);
?>

<h1>Matter Status Master Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
