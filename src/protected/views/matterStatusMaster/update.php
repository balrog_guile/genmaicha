<?php
/* @var $this MatterStatusMasterController */
/* @var $model MatterStatusMasterModel */

$this->breadcrumbs=array(
	'Matter Status Master Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MatterStatusMasterModel', 'url'=>array('index')),
	array('label'=>'Create MatterStatusMasterModel', 'url'=>array('create')),
	array('label'=>'View MatterStatusMasterModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MatterStatusMasterModel', 'url'=>array('admin')),
);
?>

<h1>Update MatterStatusMasterModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>