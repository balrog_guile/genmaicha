<?php
/* @var $this PaymentStatusMasterController */
/* @var $model PaymentStatusMasterModel */

$this->breadcrumbs=array(
	'Payment Status Master Models'=>array('index'),
	'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#payment-status-master-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>支払いステータスマスタ</h1>


<?php /*
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
*/
?>

<hr />
<?php echo CHtml::link( '新規ステータス', $this->createUrl('create'), array('class' => 'btn btn-primary' ) ); ?>
<hr />



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'payment-status-master-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'is_paid',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
