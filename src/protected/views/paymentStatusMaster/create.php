<?php
/* @var $this PaymentStatusMasterController */
/* @var $model PaymentStatusMasterModel */

$this->breadcrumbs=array(
	'Payment Status Master Models'=>array('index'),
	'Create',
);

$this->menu=array();
?>

<h1>支払いステータス作成</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>