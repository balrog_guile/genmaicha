<?php
/* @var $this PaymentStatusMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payment Status Master Models',
);

$this->menu=array(
	array('label'=>'Create PaymentStatusMasterModel', 'url'=>array('create')),
	array('label'=>'Manage PaymentStatusMasterModel', 'url'=>array('admin')),
);
?>

<h1>Payment Status Master Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
