<?php
/* @var $this PaymentStatusMasterController */
/* @var $model PaymentStatusMasterModel */

$this->breadcrumbs=array(
	'Payment Status Master Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array();
?>

<h1>支払いステータス編集</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>