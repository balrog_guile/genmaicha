<?php
/* @var $this PaymentStatusMasterController */
/* @var $model PaymentStatusMasterModel */

$this->breadcrumbs=array(
	'Payment Status Master Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PaymentStatusMasterModel', 'url'=>array('index')),
	array('label'=>'Create PaymentStatusMasterModel', 'url'=>array('create')),
	array('label'=>'Update PaymentStatusMasterModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PaymentStatusMasterModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaymentStatusMasterModel', 'url'=>array('admin')),
);
?>

<h1>View PaymentStatusMasterModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'is_paid',
	),
)); ?>
