<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>案件管理システム玄米茶</h1>

<hr />

<div class="row">
	<div class="col-md-6 alingCenter">
		<?php echo CHtml::link( '案件ステータスマスタ', Yii::app()->createUrl('matterStatusMaster'), array( 'class' => 'btn btn-default wid100') ); ?>
	</div>
	<div class="col-md-6 alingCenter">
		<?php echo CHtml::link( '支払ステータスマスタ', Yii::app()->createUrl('paymentStatusMaster'), array( 'class' => 'btn btn-default wid100') ); ?>
	</div>
</div>

<hr />

<div class="row">
	<div class="col-md-6 alingCenter">
		<?php echo CHtml::link( '顧客DB', Yii::app()->createUrl('customer'), array( 'class' => 'btn btn-primary btn-lg wid100') ); ?>
	</div>
	<div class="col-md-6 alingCenter">
		<?php echo CHtml::link( '案件DB', Yii::app()->createUrl('matter/admin'), array( 'class' => 'btn btn-primary btn-lg wid100') ); ?>
	</div>
</div>
